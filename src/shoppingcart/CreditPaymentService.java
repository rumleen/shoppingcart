/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package shoppingcart;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class CreditPaymentService extends PaymentService{

    @Override
    public void processPayment(double amount) {
        System.out.println("Processing credit payment of $" + amount);
    }
}

/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package shoppingcart;
import java.util.ArrayList;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Cart {

    private ArrayList<Product> products = new ArrayList<>();
    private PaymentService service;

    public void setPaymentService(PaymentService service) {
        this.service = service;
    }

    public void addProduct(Product product) {
        this.products.add(product);
    }

    public void payCart() {
        double total = 0.0d;
        for (Product product : products) {
            total += product.getPrice();
        }
        service.processPayment(total);
    }
}

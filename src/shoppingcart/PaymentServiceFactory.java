/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package shoppingcart;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class PaymentServiceFactory {
    private static PaymentServiceFactory instance = null;

    private PaymentServiceFactory() {
    }

    public static PaymentServiceFactory getInstance() {
        if (instance == null) {
            instance = new PaymentServiceFactory();
        }
        return instance;
    }

    public PaymentService getPaymentService(PaymentServiceType type) {
        if (type == PaymentServiceType.CREDIT) {
            return new CreditPaymentService();
        }
        if (type == PaymentServiceType.DEBIT) {
            return new DebitPaymentService();
        }
        return null;
    } 
}

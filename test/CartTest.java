/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */

import java.math.BigDecimal;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import shoppingcart.Cart;
import shoppingcart.Product;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class CartTest {
    
    private ShoppingCart cart;
   
    
    @BeforeAll
    public void setup() {
        cart = new ShoppingCart();
    }
    
    @Test
    public void CanCreateAnEmptyShoppingCart() {
        System.out.println("First Test Case:"+cart.getProductsCount());
        assertEquals(0, cart.getProductsCount());
    }
    
    @Test
    public void CanAddProductsToKart() {
        
        assertNotEquals(0, cart.getProductsCount());
        System.out.println("Second Test Case:"+cart.getProductsCount());
        assertEquals(2, cart.getProductsCount());
        assertEquals(new BigDecimal(60000.25), cart.getTotal());
        System.out.println("Second Test Case:"+cart.getTotal());
    }
    
    @Test
    public void CanCalculateTotalCartPrice() {
        System.out.println("Third Test Case:"+cart.getTotal());
        System.out.println("Third Test Case:"+cart.getProductsCount());
        assertEquals(new BigDecimal(60000.25), cart.getTotal());
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

    private static class ShoppingCart {

        public ShoppingCart() {
        }

        private String getProductsCount() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private void addProduct(Product appleProduct) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private Object getTotal() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
